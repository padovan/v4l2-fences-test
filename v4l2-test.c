/*
 * v4l2-test.c - V4L2 test framework
 *
 * Copyright (C) 2017 Collabora Ltd.
 *
 * Author: Gustavo Padovan <gustavo@padovan.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.
 */

#include <errno.h>
#include <fcntl.h>
#include <math.h>
#include <poll.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdbool.h>
#include <pthread.h>

#include "videodev2.h"
#include "sw_sync.h"
#include "sync.h"

#define ERRSTR strerror(errno)

#define BYE_ON(cond, ...) \
do { \
	if (cond) { \
		int errsv = errno; \
		fprintf(stderr, "ERROR(%s:%d) : ", \
			__FILE__, __LINE__); \
		errno = errsv; \
		fprintf(stderr,  __VA_ARGS__); \
		abort(); \
	} \
} while(0)

#define BUFFER_CNT	5

enum stream_t {
	CAPTURE = 0,
	OUTPUT,
};

struct fd_data {
	int fd;
	int fence_fd[BUFFER_CNT];
	int index;
	int first;
};

int fd[BUFFER_CNT];
int indexes[BUFFER_CNT + 5 + 30]; /* buf_cnt + std{in,out,err} + v{in,out} */
struct fd_data fds[2];
bool stream_started;

enum test_t {
	TEST_NORMAL,
	TEST_BUF_STATUS,
	TEST_NO_IN_FENCE_FLAG,
	TEST_NO_IN_FENCE_FD,
	TEST_CANCEL_STREAM,
	TEST_OUTPUT_ORDER,
	TEST_NO_FENCES,
	TEST_SW_SYNC,
};

int fd_is_valid(int fd)
{
	int status;

	if (fd == -1)
		return 1;

	status = fcntl(fd, F_GETFD, 0);
	if (status < 0)
		printf("fd %d not valid\n", fd);

	return (status >= 0);
}

void add_fence(enum stream_t type, int fd)
{
	fd_is_valid(fd);

	fds[type].fence_fd[fds[type].index++ % BUFFER_CNT] = fd;
}

int get_fence(int type)
{
	int first = fds[type].first;
	int fd;


	fd = fds[type].fence_fd[first % BUFFER_CNT];
	fds[type].fence_fd[first % BUFFER_CNT] = -1;
	fds[type].first++;

	fd_is_valid(fd);

	return fd;
}

void init_devices(int argc, char *argv[])
{
	int i, ret;

	fds[CAPTURE].fd = open(argv[1], O_RDWR);
	BYE_ON(fds[CAPTURE].fd < 0, "open failed: %s\n", ERRSTR);

	fds[OUTPUT].fd = open(argv[2], O_RDWR);
	BYE_ON(fds[OUTPUT].fd < 0, "open failed: %s\n", ERRSTR);

	/* configure desired image size */
	struct v4l2_format fmt;
	memset(&fmt, 0, sizeof fmt);
	fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE;
	fmt.fmt.pix_mp.pixelformat = V4L2_PIX_FMT_RGB565;
	int rotation = 0;
	if (argc >= 4) {
		ret = sscanf(argv[3], "%u,%u,%d",
			&fmt.fmt.pix_mp.width,
			&fmt.fmt.pix_mp.height,
			&rotation);
		BYE_ON(ret < 2, "incorrect sensor size and rotation\n");
	}

	if (rotation) {
		struct v4l2_control ctrl = {
			.id = V4L2_CID_ROTATE,
			.value = rotation,
		};
		ret = ioctl(fds[CAPTURE].fd, VIDIOC_S_CTRL, &ctrl);
		BYE_ON(ret < 0, "VIDIOC_S_CTRL failed: %s\n", ERRSTR);
	}

	/* set format struct */
	ret = ioctl(fds[CAPTURE].fd, VIDIOC_S_FMT, &fmt);
	BYE_ON(ret < 0, "VIDIOC_S_FMT failed: %s\n", ERRSTR);

	/* get format struct */
	ret = ioctl(fds[CAPTURE].fd, VIDIOC_G_FMT, &fmt);
	BYE_ON(ret < 0, "VIDIOC_G_FMT failed: %s\n", ERRSTR);
	printf("G_FMT(fds[CAPTURE].fd): width = %u, height = %u, 4cc = %.4s\n",
		fmt.fmt.pix.width, fmt.fmt.pix.height,
		(char*)&fmt.fmt.pix.pixelformat);

	if (argc >= 5) {
		struct v4l2_crop crop;
		crop.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
		ret = sscanf(argv[4], "%d,%d,%d,%d",
			&crop.c.left, &crop.c.top,
			&crop.c.width, &crop.c.height);
		BYE_ON(ret != 4, "incorrect cropping format\n");
		ret = ioctl(fds[OUTPUT].fd, VIDIOC_S_CROP, &crop);
		BYE_ON(ret < 0, "VIDIOC_S_CROP failed: %s\n", ERRSTR);
	}

	/* pass input format to output */
	fmt.type = V4L2_BUF_TYPE_VIDEO_OUTPUT_MPLANE;
	ret = ioctl(fds[OUTPUT].fd, VIDIOC_S_FMT, &fmt);
	BYE_ON(ret < 0, "VIDIOC_S_FMT failed: %s\n", ERRSTR);

	/* get format struct */
	ret = ioctl(fds[OUTPUT].fd, VIDIOC_G_FMT, &fmt);
	BYE_ON(ret < 0, "VIDIOC_G_FMT failed: %s\n", ERRSTR);
	printf("G_FMT(fds[OUTPUT].fd): width = %u, height = %u, 4cc = %.4s\n",
		fmt.fmt.pix.width, fmt.fmt.pix.height,
		(char*)&fmt.fmt.pix.pixelformat);

	/* query capabilities */
	struct v4l2_capability caps;
	ret = ioctl(fds[CAPTURE].fd, VIDIOC_QUERYCAP, &caps);
	BYE_ON(ret < 0, "VIDIOC_QUERYCAP failed: %s\n", ERRSTR);
	printf("CAPTURE Q ordered %d\n", !!(caps.capabilities & V4L2_CAP_ORDERED));

	ret = ioctl(fds[OUTPUT].fd, VIDIOC_QUERYCAP, &caps);
	BYE_ON(ret < 0, "VIDIOC_QUERYCAP failed: %s\n", ERRSTR);
	printf("OUTPUT Q ordered %d\n", !!(caps.capabilities & V4L2_CAP_ORDERED));

	/* allocate buffers */
	struct v4l2_requestbuffers rqbufs;
	rqbufs.count = BUFFER_CNT;
	rqbufs.type = V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE;
	rqbufs.memory = V4L2_MEMORY_MMAP;

	ret = ioctl(fds[CAPTURE].fd, VIDIOC_REQBUFS, &rqbufs);
	BYE_ON(ret < 0, "VIDIOC_REQBUFS failed: %s\n", ERRSTR);
	BYE_ON(rqbufs.count < BUFFER_CNT, "failed to get %d buffers\n", 
BUFFER_CNT);

	rqbufs.count = BUFFER_CNT;
	rqbufs.type = V4L2_BUF_TYPE_VIDEO_OUTPUT_MPLANE;
	rqbufs.memory = V4L2_MEMORY_DMABUF;

	ret = ioctl(fds[OUTPUT].fd, VIDIOC_REQBUFS, &rqbufs);
	BYE_ON(ret < 0, "VIDIOC_REQBUFS failed: %s\n", ERRSTR);
	BYE_ON(rqbufs.count < BUFFER_CNT, "failed to get %d buffers\n", 
BUFFER_CNT);

	struct v4l2_exportbuffer expbuf;
	struct v4l2_plane plane;
	struct v4l2_buffer buf;

	memset(indexes, -1, sizeof(int) * (BUFFER_CNT + 5));

	for (i = 0; i < BUFFER_CNT; ++i) {

		memset(&expbuf, 0, sizeof(struct v4l2_exportbuffer));
		memset(&buf, 0, sizeof(struct v4l2_buffer));
		memset(&plane, 0, sizeof(struct v4l2_plane));

		buf.index = i;
		buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE;
		buf.memory = V4L2_MEMORY_MMAP;
		buf.m.planes = &plane;
		buf.length = 1;
		buf.fence_fd = 0;

		expbuf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE;
		expbuf.index = i;
		expbuf.plane = 0;

		/* get buffer properties from a driver */
		ret = ioctl(fds[CAPTURE].fd, VIDIOC_QUERYBUF, &buf);
		BYE_ON(ret < 0, "VIDIOC_QUERYBUF for buffer %d failed: %s\n",
			buf.index, ERRSTR);
		BYE_ON(buf.memory != V4L2_MEMORY_MMAP, "bad memory type\n");
		BYE_ON(buf.length != 1, "non singular plane format\n");

		ret = ioctl(fds[CAPTURE].fd, VIDIOC_EXPBUF, &expbuf);
		BYE_ON(ret < 0, "VIDIOC_EXPBUF failed; %s\n", ERRSTR);

		indexes[expbuf.fd] = buf.index;
		fd[i] = expbuf.fd;
	}
}

void exit_devices()
{
	close(fds[CAPTURE].fd);
	close(fds[OUTPUT].fd);
}

void dump_flags(struct v4l2_buffer *buf)
{
	printf("buf: %d, flags: ", buf->index);

	if (buf->flags & V4L2_BUF_FLAG_QUEUED)
		printf("QUEUED ");
	if (buf->flags & V4L2_BUF_FLAG_DONE)
		printf("DONE ");
	if (buf->flags & V4L2_BUF_FLAG_PREPARED)
		printf("PREPARED ");
	if (buf->flags & V4L2_BUF_FLAG_IN_FENCE)
		printf("IN_FENCE ");
	if (buf->flags & V4L2_BUF_FLAG_OUT_FENCE)
		printf("OUT_FENCE ");

	printf("\n");
}

int querybuf(struct v4l2_buffer *buf, struct v4l2_plane *plane, int index)
{
	int ret;

	buf->index = index;
	buf->type = V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE;
	buf->memory = V4L2_MEMORY_MMAP;
	buf->m.planes = plane;
	buf->length = 1;
	buf->fence_fd = -1;

	/* get buffer properties from a driver */
	ret = ioctl(fds[CAPTURE].fd, VIDIOC_QUERYBUF, buf);
	BYE_ON(ret < 0, "VIDIOC_QUERYBUF for buffer %d failed: %s\n",
		buf->index, ERRSTR);
	BYE_ON(buf->memory != V4L2_MEMORY_MMAP, "bad memory type\n");
	BYE_ON(buf->length != 1, "non singular plane format\n");

	return ret;
}

int qbuf(struct v4l2_buffer *buf, enum stream_t stream, enum test_t test,
	 int sync_fd)
{
	struct v4l2_plane *plane;
	int ret, fence_fd = -1;

	if (stream == OUTPUT) {
		buf->type = V4L2_BUF_TYPE_VIDEO_OUTPUT_MPLANE;
		buf->memory = V4L2_MEMORY_DMABUF;

		plane = buf->m.planes;
		plane->m.fd = fd[buf->index];
	} else {
		buf->type = V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE;
		buf->memory = V4L2_MEMORY_MMAP;
	}

	if (test == TEST_NO_FENCES)
		goto queue;

	if (sync_fd >= 0)
		fence_fd = sync_fd;
	else if (stream_started)
		fence_fd = get_fence(!stream);

	if (fence_fd >= 0) {
		buf->fence_fd = fence_fd;
		buf->flags |= V4L2_BUF_FLAG_IN_FENCE;
	} else {
		buf->fence_fd = -1;
		buf->flags &= ~V4L2_BUF_FLAG_IN_FENCE;
	}

	switch (test) {
		case TEST_NO_IN_FENCE_FLAG:
			buf->flags &= ~V4L2_BUF_FLAG_IN_FENCE;
			break;
		case TEST_NO_IN_FENCE_FD:
			buf->fence_fd = -1;
			break;
		default:
			buf->flags |= V4L2_BUF_FLAG_OUT_FENCE;
			break;
	};

queue:
	ret = ioctl(fds[stream].fd, VIDIOC_QBUF, buf);
	if (fence_fd >= 0 &&
	    (test == TEST_NO_IN_FENCE_FLAG || test == TEST_NO_IN_FENCE_FD)) {
		if (ret < 0) {
			ret = 0;
		} else {
			ret = -1;
			errno = EINVAL;
		}

		goto err;
	}

	if (buf->fence_fd >= 1)
		add_fence(stream, buf->fence_fd);

err:
	close(fence_fd);
	return ret;
}

void queue_buffers(enum test_t test)
{
	struct v4l2_plane plane;
	struct v4l2_buffer buf;
	int ret, i;

	memset(fds[CAPTURE].fence_fd, -1, sizeof(int) * BUFFER_CNT);
	memset(fds[OUTPUT].fence_fd, -1, sizeof(int) * BUFFER_CNT);
	fds[CAPTURE].index = 0;
	fds[OUTPUT].index = 0;
	fds[CAPTURE].first = 0;
	fds[OUTPUT].first = 0;

	if (test != TEST_NO_FENCES)
		test = TEST_NORMAL;

	for (i = 0; i < BUFFER_CNT; ++i) {

		memset(&buf, 0, sizeof(struct v4l2_buffer));
		memset(&plane, 0, sizeof(struct v4l2_plane));

		querybuf(&buf, &plane, i);

		if (i < BUFFER_CNT / 2) {
			/* enqueue low-index buffers to input */
			ret = qbuf(&buf, CAPTURE, test, -1);
		} else {
			/* enqueue high-index buffers to output */
			plane.bytesused = plane.length;
			ret = qbuf(&buf, OUTPUT, test, -1);
		}

		BYE_ON(ret < 0, "VIDIOC_QBUF for buffer %d failed: %s\n",
				buf.index, ERRSTR);

	}
}

void cap_queue_buffers(enum test_t test)
{
	struct v4l2_plane plane;
	struct v4l2_buffer buf;
	int ret, i;

	memset(fds[CAPTURE].fence_fd, -1, sizeof(int) * BUFFER_CNT);
	fds[CAPTURE].index = 0;
	fds[CAPTURE].first = 0;

	if (test != TEST_NO_FENCES)
		test = TEST_NORMAL;

	for (i = 0; i < BUFFER_CNT; ++i) {

		memset(&buf, 0, sizeof(struct v4l2_buffer));
		memset(&plane, 0, sizeof(struct v4l2_plane));

		querybuf(&buf, &plane, i);

		ret = qbuf(&buf, CAPTURE, test, -1);

		BYE_ON(ret < 0, "VIDIOC_QBUF for buffer %d failed: %s\n",
				buf.index, ERRSTR);

	}
}

void cleanup_buffers()
{
	int i;

	for (i = 0; i < BUFFER_CNT; ++i) {
		if (fds[CAPTURE].fence_fd[i] >= 0)
			close(fds[CAPTURE].fence_fd[i]);
		if (fds[OUTPUT].fence_fd[i] >= 0)
			close(fds[OUTPUT].fence_fd[i]);
	}

	stream_started = false;
}

void start_streaming()
{
	int ret, type;

	type = V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE;
	ret = ioctl(fds[CAPTURE].fd, VIDIOC_STREAMON, &type);
	BYE_ON(ret < 0, "STREAMON failed: %s\n", ERRSTR);

	type = V4L2_BUF_TYPE_VIDEO_OUTPUT_MPLANE;
	ret = ioctl(fds[OUTPUT].fd, VIDIOC_STREAMON, &type);
	BYE_ON(ret < 0, "STREAMON failed: %s\n", ERRSTR);

	stream_started = true;
}

void stop_streaming()
{
	int ret, type;

	type = V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE;
	ret = ioctl(fds[CAPTURE].fd, VIDIOC_STREAMOFF, &type);
	BYE_ON(ret < 0, "STREAMON failed: %s\n", ERRSTR);

	type = V4L2_BUF_TYPE_VIDEO_OUTPUT_MPLANE;
	ret = ioctl(fds[OUTPUT].fd, VIDIOC_STREAMOFF, &type);
	BYE_ON(ret < 0, "STREAMON failed: %s\n", ERRSTR);
}

void run_pipeline(int stop, enum test_t test)
{
	/* setup polling */
	struct pollfd poll_fd[2] = {
		{ .fd = fds[CAPTURE].fd, .events = POLLIN },
		{ .fd = fds[OUTPUT].fd, .events = POLLOUT },
	};
	struct v4l2_buffer buf;
	struct v4l2_plane plane;
	int ret, count = 0, fence_fd = -1;

	while ((ret = poll(poll_fd, 2, 5000)) > 0) {
		memset(&buf, 0, sizeof buf);
		memset(&plane, 0, sizeof plane);
		buf.m.planes = &plane;
		buf.length = 1;
		buf.fence_fd = -1;

		if (stop && stop == count)
			return;

		if (poll_fd[0].revents & POLLIN) {
			/* dequeue buffer */
			buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE;
			buf.memory = V4L2_MEMORY_MMAP;
			ret = ioctl(fds[CAPTURE].fd, VIDIOC_DQBUF, &buf);
			BYE_ON(ret, "VIDIOC_DQBUF failed: %s\n", ERRSTR);

			/* enqueue buffer */
			ret = qbuf(&buf, OUTPUT, test, fence_fd);
			BYE_ON(ret, "VIDIOC_QBUF failed: %s\n", ERRSTR);

			fence_fd = -1;
		}

		if (poll_fd[1].revents & POLLOUT) {
			/* dequeue buffer */
			buf.type = V4L2_BUF_TYPE_VIDEO_OUTPUT_MPLANE;
			buf.memory = V4L2_MEMORY_DMABUF;
			ret = ioctl(fds[OUTPUT].fd, VIDIOC_DQBUF, &buf);
			BYE_ON(ret, "VIDIOC_DQBUF failed: %s\n", ERRSTR);

			/* enqueue buffer */
			ret = qbuf(&buf, CAPTURE, test, fence_fd);
			BYE_ON(ret, "VIDIOC_QBUF failed: %d %s\n", ret, ERRSTR);

			fence_fd = -1;
		}

		count++;
	}

	BYE_ON(ret == 0, "polling timeout\n");
	BYE_ON(1, "polling stopped: %s\n", ERRSTR);
}

void run_test(int loop, int stop, enum test_t test, const char *name)
{
	int i;

	printf("test %s:\n", name);

	for (i = 0 ; i < loop ; i++) {
		queue_buffers(test);
		start_streaming();
		run_pipeline(stop, test);
		stop_streaming();
		cleanup_buffers();
	}
}

static bool use_sw_sync(enum test_t test)
{
	if (test == TEST_BUF_STATUS || test == TEST_CANCEL_STREAM ||
	    test == TEST_OUTPUT_ORDER || test == TEST_SW_SYNC)
		return true;

	return false;
}

void run_capture(int stop, enum test_t test)
{
	/* setup polling */
	struct pollfd poll_fd[1] = {
		{ .fd = fds[CAPTURE].fd, .events = POLLIN },
	};
	struct v4l2_buffer buf;
	struct v4l2_plane plane;
	int ret, count = 0, output = 0, timeline, index, n = 1, fence_fd = -1;

	if (use_sw_sync(test))
		timeline = sw_sync_timeline_create();

	while ((ret = poll(poll_fd, 2, 5000)) > 0) {
		memset(&buf, 0, sizeof buf);
		memset(&plane, 0, sizeof plane);
		buf.m.planes = &plane;
		buf.length = 1;
		buf.fence_fd = -1;

		if (stop && stop == count)
			return;

		if (poll_fd[0].revents & POLLIN) {
			/* dequeue buffer */
			buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE;
			buf.memory = V4L2_MEMORY_MMAP;
			ret = ioctl(fds[CAPTURE].fd, VIDIOC_DQBUF, &buf);
			BYE_ON(ret, "VIDIOC_DQBUF failed: %s\n", ERRSTR);

			if (test == TEST_OUTPUT_ORDER || test == TEST_SW_SYNC ||
			    test == TEST_BUF_STATUS || test == TEST_CANCEL_STREAM)
				fence_fd = sw_sync_fence_create(timeline, "test-sync", n++);

			/* enqueue buffer */
			ret = qbuf(&buf, CAPTURE, test, fence_fd);
			BYE_ON(ret, "VIDIOC_QBUF failed: %s\n", ERRSTR);

			switch (test) {
			case TEST_BUF_STATUS:
				BYE_ON(!(buf.flags & V4L2_BUF_FLAG_IN_FENCE), "Fence already signaled");

				sw_sync_timeline_inc(timeline, 1);

				index = buf.index;
				memset(&buf, 0, sizeof(struct v4l2_buffer));
				memset(&plane, 0, sizeof(struct v4l2_plane));
				querybuf(&buf, &plane, index);

				BYE_ON(buf.flags & V4L2_BUF_FLAG_IN_FENCE, "Fence didn't signal");
				break;

			case TEST_SW_SYNC:
				sw_sync_timeline_inc(timeline, 1);
				break;

			case TEST_OUTPUT_ORDER:
				if (fence_fd >= 0 && output > 0) {
					sw_sync_timeline_inc(timeline, 1);
					if (output == 2)
						sw_sync_timeline_inc(timeline, 1);
				}
				break;
			default:
				break;
			};
			output++;
			fence_fd = -1;
		}

		count++;
	}

	if (test == TEST_OUTPUT_ORDER)
		sw_sync_timeline_inc(timeline, 3);

	if (use_sw_sync(test))
		close(timeline);

	BYE_ON(ret == 0, "polling timeout\n");
	BYE_ON(1, "polling stopped: %s\n", ERRSTR);
}

void run_test_cap(int loop, int stop, enum test_t test, const char *name)
{
	int i, ret, type;
	printf("test %s:\n", name);

	for (i = 0 ; i < loop ; i++) {
		cap_queue_buffers(test);

		type = V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE;
		ret = ioctl(fds[CAPTURE].fd, VIDIOC_STREAMON, &type);
		BYE_ON(ret < 0, "STREAMON failed: %s\n", ERRSTR);

		run_capture(stop, test);

		type = V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE;
		ret = ioctl(fds[CAPTURE].fd, VIDIOC_STREAMOFF, &type);
		BYE_ON(ret < 0, "STREAMON failed: %s\n", ERRSTR);
		cleanup_buffers();
	}
}

int main(int argc, char *argv[])
{
	BYE_ON(argc < 3, "bad args:\n\t%s input-node output-node "
		"[w,h,rot] [left,top,w,h]\n", argv[0]);

	init_devices(argc, argv);
	/*
	run_test(10, 100, TEST_NO_FENCES, "no-fences");
	run_test(100, 10, TEST_NORMAL, "normal-1000*10");
	run_test(10, 1000, TEST_NORMAL, "normal-10*1000");
	*/
	run_test_cap(1, 1, TEST_NO_IN_FENCE_FLAG, "no-in-fence-flag");
	run_test_cap(1, 1, TEST_NO_IN_FENCE_FD, "no-in-fence-fd");
	run_test_cap(2,30, TEST_NO_FENCES, "normal-capture");
	run_test_cap(1, 30, TEST_SW_SYNC, "sw_sync");
	run_test_cap(1, 3, TEST_CANCEL_STREAM, "cancel-stream");
	run_test_cap(1, 2, TEST_BUF_STATUS, "buf-status");
	run_test_cap(1, 10, TEST_OUTPUT_ORDER, "output-order");

	exit_devices();

	printf("END\n");

	return 0;
}
