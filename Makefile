CC=gcc
CFLAGS=-c -Wall -g
LDFLAGS= -lpthread
SOURCES=v4l2-test.c sync.c
OBJECTS=$(SOURCES:.c=.o)
EXECUTABLE=v4l2-test

all: $(SOURCES) $(EXECUTABLE)
	    
$(EXECUTABLE): $(OBJECTS) 
	    $(CC) $(LDFLAGS) $(OBJECTS) -o $@

.c.o:
	    $(CC) $(CFLAGS) $< -o $@
